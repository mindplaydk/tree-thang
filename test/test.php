<?php

use cms\HandlerInterface;
use cms\Node;
use cms\NodeRepo;
use cms\Schema;
use mindplay\sql\Connection;

require __DIR__ . '/header.php';

/**
 * @return Connection
 */
function create_db()
{
    static $db;

    if ($db === null) {
        $db = new Connection(new PDO("mysql:host=localhost;dbname=cms", "root", ""));
    }

    return $db;
}

/**
 * @return NodeRepo
 */
function create_repo() {
    $pdo = create_db();

    $schema = new Schema();

    $schema->addNodeType("product", [DocumentComponent::class]);

    $schema->addHandlerType(DocumentComponent::class, DocumentHandler::class);

    return new NodeRepo($pdo, $schema);
}

function truncate_tables() {
    create_db()->exec("TRUNCATE TABLE nodes");
    create_db()->exec("TRUNCATE TABLE node_parents");
}

class DocumentComponent
{
    /** @var int owner Node ID */
    public $node_id;

    /** @var string document title */
    public $title;

    /** @var string document body */
    public $body;
}

class DocumentHandler implements HandlerInterface
{
    public function loadComponents(array $node_ids)
    {
        return array_map(
            function ($node_id) {
                $comp = new DocumentComponent();

                $comp->node_id = $node_id;

                return $comp;
            },
            $node_ids
        );
    }

    public function saveComponent($node_id, $component)
    {

    }
}

//class MockPDO extends PDO
//{
//    public $queries = [];
//
//    public function exec($statement)
//    {
//        $this->queries[] = $statement;
//    }
//
//
//}

function check_path(array $nodes, array $names) {
    eq(array_map(function (Node $n) { return $n->name; }, $nodes), $names);
}

test(
    "can find node by path",
    function () {
        truncate_tables();

        $repo = create_repo();

        $repo->createPath("/food");
        $repo->createPath("/food/fruit");
        $repo->createPath("/food/fruit/red/cherry");
        $repo->createPath("/food/fruit/yellow/banana");
        $repo->createPath("/food/meat/beef");
        $repo->createPath("/food/meat/pork");

        check_path($repo->listPath("/food"), ["food"]);
        check_path($repo->listPath("/food/fruit"), ["food", "fruit"]);
        check_path($repo->listPath("/food/fruit/red/cherry"), ["food", "fruit", "red", "cherry"]);
        check_path($repo->listPath("/food/fruit/yellow/banana"), ["food", "fruit", "yellow", "banana"]);
    }
);

//test(
//    "can load/save components",
//    function () {
//        truncate_tables();
//
//        $repo = create_repo();
//
//
//    }
//);

exit(run());
