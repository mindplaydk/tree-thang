<?php

namespace cms;

class Schema
{
    /**
     * @var NodeType[] map where type-name => NodeType instance
     */
    public $node_types = [];

    /**
     * @var string[] map where component class-name => handler class-name
     */
    public $handler_types = [];

    /**
     * Define a Node type.
     *
     * A Node type is a name associated with a list of component-types - these components
     * will be validated, when saving a Node of that type.
     *
     * @param string   $type_name       Node type-name
     * @param string[] $component_types list of associated component class-names
     *
     * @return void
     */
    public function addNodeType($type_name, array $component_types = [])
    {
        $type = new NodeType();

        $type->name = $type_name;
        $type->component_types = $component_types;

        $this->node_types[$type->name] = $type;
    }

    /**
     * Define a handler-type for a component-type.
     *
     * @see HandlerInterface
     *
     * @param string $component_type component type class-name
     * @param string $handler_type   handler type class-name
     *
     * @return void
     */
    public function addHandlerType($component_type, $handler_type)
    {
        $this->handler_types[$component_type] = $handler_type;
    }
}
