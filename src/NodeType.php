<?php

namespace cms;

/**
 * This internal model defines a list of components associated with a given Node type
 */
class NodeType
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string[] list of component types
     */
    public $component_types = array();
}
