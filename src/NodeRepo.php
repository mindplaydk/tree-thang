<?php

namespace cms;

use InvalidArgumentException;
use mindplay\sql\Connection;

class NodeRepo
{
    /**
     * @var Connection
     */
    private $db;

    /**
     * @var Node absolute root Node (exists in-memory only)
     */
    private $root;

    /**
     * @var Schema
     */
    private $schema;

    public function __construct(Connection $db, Schema $schema)
    {
        $this->pdo = $pdo;
        $this->schema = $schema;

        $root = new Node();
        $root->id = null;
        $root->parent_id = null;
        $root->name = '';
        $root->type = null;
        $root->v_depth = -1;
        $root->h_index = 0;

        $this->root = $root;
    }

    /**
     * Create and/or resolve a Node by it's absolute path.
     * Parent and child Nodes will be created as needed.
     *
     * @param string $path absolute Node path
     *
     * @return Node|null the Node corresponding to the given path
     */
    public function createPath($path)
    {
        $names = $this->getNames($path);

        $parent_id = null;

        $node = null;

        foreach ($names as $v_depth => $name) {
            $params = [
                "name" => $name,
            ];

            if ($parent_id === null) {
                $parent_cond = "n.parent_id IS NULL";
            } else {
                $parent_cond = "n.parent_id = :parent_id";
                $params["parent_id"] = $parent_id;
            }

            $sql = "SELECT n.* FROM nodes n WHERE {$parent_cond} AND n.name = :name";

            $nodes = $this->fetchNodes($sql, $params);

            if (isset($nodes[0])) {
                $node = $nodes[0];
            } else {
                $node = $this->createNode($node ?: $this->root, $name);
            }

            $parent_id = $node->id;
        }

        return $node;
    }

    /**
     * Get a list of Nodes that make up a given absolute path.
     *
     * @param string $path absolute Node path
     *
     * @return Node[] list of path Nodes, in parent -> child order
     */
    public function listPath($path)
    {
        // TODO benchmark this vs a simpler, multiple-round-trip approach (as implemented by createPath)

        $names = $this->getNames($path);

        $select = [];
        $from = [];
        $where = [];
        $params = [];
        $prev_alias = null;

        foreach ($names as $v_depth => $name) {
            $alias = "n{$v_depth}";

            $select[] = "{$alias}.id {$alias}_id, {$alias}.type {$alias}_type, {$alias}.h_index {$alias}_h_index";

            $from[] = $v_depth === 0
                ? "nodes {$alias}"
                : "INNER JOIN nodes {$alias} ON {$alias}.parent_id = {$prev_alias}.id";

            $where[] = "{$alias}.name = :name_{$v_depth}";

            $params["name_{$v_depth}"] = $name;

            $prev_alias = $alias;
        }

        $select = implode("\n    , ", $select);
        $from = implode("\n    ", $from);
        $where = implode("\n    AND ", $where);

        $sql = "SELECT {$select}\nFROM {$from}\nWHERE {$where}";

        $q = new RecordSet($this->pdo, $sql, $params);

        $row = $q->firstRow();

        if ($row === null) {
            return null;
        }

        $nodes = [];

        foreach ($names as $v_depth => $name) {
            $nodes[] = $this->hydrate([
                "id"        => $row["n{$v_depth}_id"],
                "h_index"   => $row["n{$v_depth}_h_index"],
                "name"      => $names[$v_depth],
                "type"      => $row["n{$v_depth}_type"],
                "parent_id" => $v_depth > 0 ? $names[$v_depth - 1] : null,
                "v_depth"   => $v_depth,
            ]);
        }

        return $nodes;
    }

    /**
     * @param string $sql
     *
     * @return Node[]
     */
    protected function fetchNodes($sql, array $params)
    {
        $nodes = [];

        $q = new RecordSet($this->pdo, $sql, $params);

        foreach ($q as $row) {
            $nodes[] = $this->hydrate($row);
        }

        return $nodes;
    }

    /**
     * Create a new Node under a given parent Node
     *
     * @param Node   $parent
     * @param string $name
     *
     * @return Node
     */
    protected function createNode(Node $parent, $name)
    {
        $this->pdo->beginTransaction();

        if ($parent->id !== null) {
            $q = new RecordSet(
                $this->pdo,
                "SELECT MAX(n.h_index) FROM nodes n WHERE n.parent_id = :parent_id LOCK IN SHARE MODE",
                ["parent_id" => $parent->id]
            );

            $index = $q->firstCol();

            $h_index = ($index === null ? -1 : (int) $index) + 1;
        } else {
            $h_index = 0;
        }

        $node = new Node();

        $node->name = $name;
        $node->parent_id = $parent->id;
        $node->v_depth = $parent->v_depth + 1;
        $node->h_index = $h_index;

        $this->insertNode($node);

        if ($parent->id !== null) {
            // copy parent indices:

            $q = new RecordSet(
                $this->pdo,
                "INSERT INTO node_parents (node_id, ancestor_id, distance)"
                . " SELECT :node_id, p.ancestor_id, p.distance + 1 FROM node_parents p"
                . " WHERE p.node_id = :parent_id",
                ["node_id" => $node->id, "parent_id" => $node->parent_id]
            );

            $q->exec();

            // create parent index:

            $q = new RecordSet(
                $this->pdo,
                "INSERT INTO node_parents (node_id, ancestor_id, distance) VALUES (:node_id, :parent_id, :distance)",
                ["node_id" => $node->id, "parent_id" => $node->parent_id, "distance" => 1]
            );

            $q->exec();
        }

        $this->pdo->commit();

        return $node;
    }

    /**
     * @param $path
     *
     * @return array
     */
    protected function getNames($path)
    {
        return explode("/", trim($path, "/"));
    }

    /**
     * @param array $row database row
     *
     * @return Node
     */
    protected function hydrate($row)
    {
        $node = new Node();

        $node->id = (int) $row['id'];
        $node->parent_id = $row['parent_id'] === null ? null : (int) $row['parent_id'];
        $node->name = $row['name'];
        $node->type = $row['type'];
        $node->v_depth = (int) $row['v_depth'];
        $node->h_index = (int) $row['h_index'];

        return $node;
    }

    /**
     * Internally INSERT a new Node into the database
     *
     * @param Node $node
     */
    private function insertNode(Node $node)
    {
        if ($node->id !== null) {
            throw new InvalidArgumentException("\$node->id is not NULL");
        }

        $q = new RecordSet(
            $this->pdo,
            "INSERT INTO nodes (parent_id, name, type, v_depth, h_index) VALUES (:parent_id, :name, :type, :v_depth, :h_index)",
            $data = [
                "parent_id" => $node->parent_id,
                "name"      => $node->name,
                "type"      => $node->type,
                "v_depth"   => $node->v_depth,
                "h_index"   => $node->h_index,
            ]
        );

        $q->exec();

        $node->id = (int) $this->pdo->lastInsertId();
    }
}
