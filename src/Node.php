<?php

namespace cms;

class Node
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int|null
     */
    public $parent_id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $type;

    /**
     * @var int
     */
    public $v_depth;

    /**
     * @var int
     */
    public $h_index;

    /**
     * @var object[] map where component class-name => component instance
     */
    public $components = [];
}
