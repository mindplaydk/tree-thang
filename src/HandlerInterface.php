<?php

namespace cms;

/**
 * A handler is responsible for various aspects of handling a type of component.
 */
interface HandlerInterface
{
    /**
     * @param int[] $node_ids
     *
     * @return object[]
     */
    public function loadComponents(array $node_ids);

    /**
     * @param int $node_id
     * @param object $component
     *
     * @return void
     */
    public function saveComponent($node_id, $component);
}
