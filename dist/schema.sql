CREATE TABLE node_parents
(
  node_id INT UNSIGNED NOT NULL,
  ancestor_id INT UNSIGNED,
  distance SMALLINT
);
CREATE TABLE nodes
(
  id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  parent_id INT UNSIGNED,
  name VARCHAR(255) NOT NULL,
  type VARCHAR(255),
  v_depth SMALLINT UNSIGNED NOT NULL,
  h_index INT UNSIGNED NOT NULL
);
CREATE UNIQUE INDEX unique_child ON nodes (parent_id, name);
CREATE UNIQUE INDEX unique_child_index ON nodes (parent_id, h_index);
CREATE INDEX node_type ON nodes (type);
